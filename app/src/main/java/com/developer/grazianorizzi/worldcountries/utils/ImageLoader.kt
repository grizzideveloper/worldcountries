package com.developer.grazianorizzi.worldcountries.utils

import android.widget.ImageView
import com.squareup.picasso.Picasso

class ImageLoader private constructor(){

    private val picasso:Picasso = Picasso.get()

    companion object{
        private val instance:ImageLoader = ImageLoader()

        fun loadCountryFlag(imageView: ImageView,coutryCode:String){
            val url = "https://www.countryflags.io/${coutryCode.toLowerCase()}/flat/64.png"
            instance.picasso.load(url).into(imageView)
        }
    }

}