package com.developer.grazianorizzi.worldcountries.entity

import java.io.Serializable


data class Language(var languageName:String):Serializable{
    override fun toString(): String {
        return languageName
    }
}