package com.developer.grazianorizzi.worldcountries.model

import com.developer.grazianorizzi.worldcountries.entity.Country
import com.developer.grazianorizzi.worldcountries.entity.Filter
import com.developer.grazianorizzi.worldcountries.model.convertes.CountryDTOtoCountryConverter
import com.developer.grazianorizzi.worldcountries.model.convertes.FilterDTOtoFilterConverter
import com.developer.grazianorizzi.worldcountries.model.data.LocalRepository
import com.developer.grazianorizzi.worldcountries.model.data.LocalRepositoryImp
import com.developer.grazianorizzi.worldcountries.model.ws.Client
import com.developer.grazianorizzi.worldcountries.model.ws.ClientImp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CountriesListModel {

    val client:Client = ClientImp.instance
    val repository:LocalRepository = LocalRepositoryImp()

    val countryConverter = CountryDTOtoCountryConverter()
    val filterConverter = FilterDTOtoFilterConverter()


    suspend fun getAllCountries():ArrayList<Country> = withContext(Dispatchers.IO){
        val arr = client.getAllWorldCountry() ?: ArrayList()
        countryConverter.convert(arr)
    }


    suspend fun getCountriesByLanguage(language:Filter):ArrayList<Country> = withContext(Dispatchers.IO){
        val arr = client.getWorldCountryByLanguage(language.code) ?: ArrayList()
        countryConverter.convert(arr)
    }


    suspend fun getCountriesByContinent(continent:Filter):ArrayList<Country> = withContext(Dispatchers.IO){
        val arr = client.getWorldCountryByContinent(continent.code) ?: ArrayList()
        countryConverter.convert(arr)
    }


    suspend fun getLanguageFilter():ArrayList<Filter> = withContext(Dispatchers.IO){
        filterConverter.convert(repository.getFilterLanguage())
    }


    suspend fun getContinentFilter():ArrayList<Filter> = withContext(Dispatchers.IO){
        filterConverter.convert(repository.getFilterContinent())
    }
}