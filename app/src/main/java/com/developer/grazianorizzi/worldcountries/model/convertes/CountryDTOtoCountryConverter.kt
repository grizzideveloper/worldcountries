package com.developer.grazianorizzi.worldcountries.model.convertes

import com.developer.grazianorizzi.worldcountries.entity.Country
import com.developer.grazianorizzi.worldcountries.model.dto.CountryDTO

class CountryDTOtoCountryConverter {

    fun convert(countriesDTO:ArrayList<CountryDTO>):ArrayList<Country>{
        val countries:ArrayList<Country> = ArrayList()
        countriesDTO.forEach{
            countries.add(Country(it.name,it.countryCode,it.capital))
        }
        return countries
    }
}