package com.developer.grazianorizzi.worldcountries.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.developer.grazianorizzi.worldcountries.R
import com.developer.grazianorizzi.worldcountries.entity.Language
import kotlinx.android.synthetic.main.language_item_list.view.*

class MyLanguageRecyclerViewAdapter(
    private var mValues: List<Language>,
    private val mListener: OnLanguageListItemClick?
) : RecyclerView.Adapter<MyLanguageRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Language
            mListener?.onLanguageItemClick(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.language_item_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]

        with(holder) {
            languageName.text = item.languageName
            with(mView) {
                tag = item
                setOnClickListener(mOnClickListener)
            }
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val languageName: TextView = mView.language_name

        override fun toString(): String {
            return super.toString() + " '" + languageName.text + "'"
        }
    }

    interface OnLanguageListItemClick {
        fun onLanguageItemClick(language: Language)
    }
}

