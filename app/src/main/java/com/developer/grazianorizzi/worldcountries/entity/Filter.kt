package com.developer.grazianorizzi.worldcountries.entity

import java.io.Serializable


data class Filter(
    var name:String,
    var code:String
):Serializable