package com.developer.grazianorizzi.worldcountries.view.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.developer.grazianorizzi.worldcountries.R
import com.developer.grazianorizzi.worldcountries.entity.Country
import com.developer.grazianorizzi.worldcountries.utils.ImageLoader
import com.developer.grazianorizzi.worldcountries.view.fragments.CountriesFragment.OnListFragmentInteractionListener
import kotlinx.android.synthetic.main.fragment_country.view.*

/**
 * [RecyclerView.Adapter] that can display a [Country] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class MyCountriesRecyclerViewAdapter(
    private var mValues: List<Country>,
    private val mListener: OnListFragmentInteractionListener?
) : RecyclerView.Adapter<MyCountriesRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Country
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onListFragmentInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_country, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]

        with(holder){
            countryName.text = item.name
            capitalName.text = item.capital
            ImageLoader.loadCountryFlag(countryFlag,item.countryCode)
            with(mView) {
                tag = item
                setOnClickListener(mOnClickListener)
            }
        }
    }

    override fun getItemCount(): Int = mValues.size

    fun update(countries:List<Country>){
        mValues = countries
        this.notifyDataSetChanged()
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val countryName: TextView = mView.country_name
        val capitalName: TextView = mView.capital_name
        val countryFlag: ImageView = mView.country_flag

        override fun toString(): String {
            return super.toString() + " '" + countryName.text + "'"
        }
    }
}

