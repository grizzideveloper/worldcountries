package com.developer.grazianorizzi.worldcountries.model

import com.developer.grazianorizzi.worldcountries.entity.Country
import com.developer.grazianorizzi.worldcountries.entity.CountryDetail
import com.developer.grazianorizzi.worldcountries.model.convertes.CountryDetailDTOtoCountryDetail
import com.developer.grazianorizzi.worldcountries.model.ws.Client
import com.developer.grazianorizzi.worldcountries.model.ws.ClientImp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CountryDetailModel {

    val client: Client = ClientImp.instance
    val converter:CountryDetailDTOtoCountryDetail = CountryDetailDTOtoCountryDetail()

    suspend fun getCountryDetail(country:Country): CountryDetail = withContext(Dispatchers.IO){
        var res = client.getCountry(country.countryCode)!!
        converter.convert(res)
    }
}