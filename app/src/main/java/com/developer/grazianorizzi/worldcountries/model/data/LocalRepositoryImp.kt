package com.developer.grazianorizzi.worldcountries.model.data

import android.content.Context
import com.developer.grazianorizzi.worldcountries.MainApplication
import com.developer.grazianorizzi.worldcountries.model.dto.FilterDTO
import com.developer.grazianorizzi.worldcountries.utils.ReadLocalDataException
import com.google.gson.Gson
import java.io.InputStream

class LocalRepositoryImp(
    private val context: Context = MainApplication.applicationContext(),
    private val LANGUAGE_FILE_NAME: String = "language_filter",
    private val CONTINENT_FILE_NAME: String = "continent_filter"
) : LocalRepository {

    private val gson: Gson = Gson()

    private var languagesFilter: Array<FilterDTO>? = null
    private var continentFilter: Array<FilterDTO>? = null

    override suspend fun getFilterLanguage(): Array<FilterDTO> {
        try {
            if (languagesFilter == null) {
                languagesFilter = gson.fromJson(readJSONFromAsset(LANGUAGE_FILE_NAME), Array<FilterDTO>::class.java)
            }
        }catch (ex:Throwable){
            throw ReadLocalDataException(ex)
        }
        return languagesFilter!!
    }

    override suspend fun getFilterContinent(): Array<FilterDTO> {
        try {
            if (continentFilter == null) {
                continentFilter = gson.fromJson(readJSONFromAsset(CONTINENT_FILE_NAME), Array<FilterDTO>::class.java)
            }
        }catch (ex:Throwable){
            throw ReadLocalDataException(ex)
        }
        return continentFilter!!
    }

    private fun readJSONFromAsset(path: String): String? {
        var json = ""
        val inputStream: InputStream = context.assets.open(path + ".json")
        json = inputStream.bufferedReader().use { it.readText() }
        return json
    }
}