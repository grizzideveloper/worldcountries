package com.developer.grazianorizzi.worldcountries.model.dto

import com.google.gson.annotations.SerializedName

data class FilterDTO(
    @SerializedName("name") var name:String,
    @SerializedName("code") var code:String
)