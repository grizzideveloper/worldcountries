package com.developer.grazianorizzi.worldcountries.view.fragments

import android.app.Application
import android.app.ProgressDialog
import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateViewModelFactory
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.developer.grazianorizzi.worldcountries.R
import com.developer.grazianorizzi.worldcountries.entity.Country
import com.developer.grazianorizzi.worldcountries.utils.ErrorHandler
import com.developer.grazianorizzi.worldcountries.view.MySpinner
import com.developer.grazianorizzi.worldcountries.view.adapters.MyCountriesRecyclerViewAdapter
import com.developer.grazianorizzi.worldcountries.view.adapters.MySpinnerAdapter
import com.developer.grazianorizzi.worldcountries.viewmodel.CountriesListViewModel
import com.developer.grazianorizzi.worldcountries.viewmodel.LoadableViewModel
import kotlinx.android.synthetic.main.fragment_countries_list.*
import kotlinx.android.synthetic.main.fragment_countries_list.view.*

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [CountriesFragment.OnListFragmentInteractionListener] interface.
 */
class CountriesFragment : Fragment() {

    private var columnCount = 1
    private lateinit var viewModel: CountriesListViewModel
    private lateinit var progressDialog: ProgressDialog
    private lateinit var errorHandler: ErrorHandler
    private lateinit var myAdapter: MyCountriesRecyclerViewAdapter
    private lateinit var mySpinnerAdapter: MySpinnerAdapter
    private var listener: OnListFragmentInteractionListener? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_countries_list, container, false)
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            columnCount = 2
        }
        myAdapter =
            MyCountriesRecyclerViewAdapter(
                ArrayList(),
                listener
            )
        // Set the myAdapter
        with(view.list) {
            layoutManager = when {
                columnCount <= 1 -> LinearLayoutManager(context)
                else -> GridLayoutManager(context, columnCount)
            }
            adapter = myAdapter
        }

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, SavedStateViewModelFactory(context?.applicationContext as Application,this)).get(CountriesListViewModel::class.java)

        progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Loading")
        progressDialog.setCancelable(false)

        errorHandler = ErrorHandler(context)

        mySpinnerAdapter = MySpinnerAdapter(context)
        spinner.adapter = mySpinnerAdapter

        viewModel.state.observe(this, Observer {
            if (it == LoadableViewModel.ViewModelState.LOADING) {
                progressDialog.show()
            } else {
                progressDialog.hide()
            }
        })

        no_filter.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                viewModel.removeFilter()
                spinner.visibility = GONE
            }
        }

        language_filter.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                viewModel.setLanguageFilter()
                spinner.visibility = VISIBLE
            }
        }

        continent_filter.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                viewModel.setContinentFilter()
                spinner.visibility = VISIBLE
            }
        }

        when(viewModel.filterState){
            CountriesListViewModel.FilterState.NONE  -> {
                radioGroup.check(R.id.no_filter)
            }
            CountriesListViewModel.FilterState.LANGUAGE -> {
                radioGroup.check(R.id.language_filter)
            }
            CountriesListViewModel.FilterState.CONTINENT -> {
                radioGroup.check(R.id.continent_filter)
            }
            CountriesListViewModel.FilterState.VOID ->{
                radioGroup.check(R.id.no_filter)
            }
        }

        spinner.itemSelectionListener = object :
            MySpinner.ItemSelection {
            override fun onItemSelected(position: Int) {
                mySpinnerAdapter.getItem(position)?.let {
                    viewModel.filterCountry(it)
                }
            }
        }

        viewModel.countriesList.observe(this, Observer {
            //Aggiorna lista
            myAdapter.update(it)
        })

        viewModel.errorRecived.observe(this, Observer {
            //Show error
            it.consume()?.let { t ->
                errorHandler.handleError(t)
            }
        })

        viewModel.filters.observe(this, Observer {filters ->
            mySpinnerAdapter.clear()
            mySpinnerAdapter.addAll(filters)
            if (filters.size > 0){
                viewModel.selectedFilter?.let {
                    val pos = filters.indexOf(it)
                    if (pos > -1) spinner.setSelection(pos)
                    else spinner.setSelection(0)
                } ?: run{
                    spinner.setSelection(0)
                }
            }
        })
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: Country?)
    }

    companion object {
        @JvmStatic
        fun newInstance() = CountriesFragment()
    }
}
