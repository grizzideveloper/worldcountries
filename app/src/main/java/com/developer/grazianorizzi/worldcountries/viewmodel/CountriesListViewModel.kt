package com.developer.grazianorizzi.worldcountries.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.developer.grazianorizzi.worldcountries.entity.Country
import com.developer.grazianorizzi.worldcountries.entity.Filter
import com.developer.grazianorizzi.worldcountries.model.CountriesListModel


class CountriesListViewModel(private val savedState: SavedStateHandle) : LoadableViewModel() {

    val countriesList: MutableLiveData<ArrayList<Country>> =
        savedState.getLiveData("COUNTRIES_LIST", ArrayList())
    val filters: MutableLiveData<ArrayList<Filter>> = savedState.getLiveData("filters", ArrayList())
    var filterState: FilterState = savedState.get<FilterState>("filtersState") ?: FilterState.VOID
        set(value) {
            field = value
            savedState.set("filtersState", value)
        }
    var selectedFilter: Filter? = savedState.get<Filter>("selectedFilter")
        set(value) {
            field = value
            savedState.set("selectedFilter", value)
        }

    private val model: CountriesListModel = CountriesListModel()


    fun fetchAllCountries() {
        launchOnViewModelScope {
            countriesList.value = model.getAllCountries()
            savedState.set("COUNTRIES_LIST", countriesList.value)
        }
    }

    fun fetchContinentFilter() {
        launchOnViewModelScope {
            changeFilter(FilterState.CONTINENT)
            filters.value = model.getContinentFilter()
            savedState.set("filters", filters.value)
        }
    }

    fun fetchLanguageFilter() {
        launchOnViewModelScope {
            changeFilter(FilterState.LANGUAGE)
            filters.value = model.getLanguageFilter()
            savedState.set("filters", filters.value)
        }

    }

    private fun changeFilter(filterState: FilterState) {
        filters.value = ArrayList()
        countriesList.value = ArrayList()
        this.filterState = filterState
    }

    private fun fetchCountryForLanguage(languageFilter: Filter) {
        launchOnViewModelScope {
            countriesList.value = model.getCountriesByLanguage(languageFilter)
            savedState.set("COUNTRIES_LIST", countriesList.value)
        }
    }

    private fun fetchCountryForContinent(continentFilter: Filter) {
        launchOnViewModelScope {
            countriesList.value = model.getCountriesByContinent(continentFilter)
            savedState.set("COUNTRIES_LIST", countriesList.value)
        }
    }

    fun filterCountry(filter: Filter) {
        if (selectedFilter != filter) {
            selectedFilter = filter
            when (filterState) {
                FilterState.LANGUAGE -> fetchCountryForLanguage(filter)
                FilterState.CONTINENT -> fetchCountryForContinent(filter)
                else -> this.removeFilter()
            }
        }
    }

    fun setLanguageFilter() {
        if (filterState != FilterState.LANGUAGE) {
            fetchLanguageFilter()
        }
    }

    fun setContinentFilter() {
        if (filterState != FilterState.CONTINENT) {
            fetchContinentFilter()
        }
    }

    fun removeFilter() {
        if (filterState != FilterState.NONE) {
            filterState = FilterState.NONE
            selectedFilter = null
            fetchAllCountries()
        }
    }

    enum class FilterState {
        NONE,
        LANGUAGE,
        CONTINENT,
        VOID
    }
}
