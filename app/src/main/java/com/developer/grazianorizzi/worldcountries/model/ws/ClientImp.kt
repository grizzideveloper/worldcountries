package com.developer.grazianorizzi.worldcountries.model.ws

import com.developer.grazianorizzi.worldcountries.model.dto.CountryDTO
import com.developer.grazianorizzi.worldcountries.model.dto.CountryDetailDTO
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ClientImp private constructor():Client{


    private val httpClient = OkHttpClient().newBuilder()
        .addInterceptor {
            it.proceed(it.request())
        }
        .connectTimeout(5, TimeUnit.SECONDS)
        .readTimeout(5, TimeUnit.SECONDS)
        .writeTimeout(5, TimeUnit.SECONDS)
        .build()

    private val rClient: Retrofit = Retrofit.Builder()
        .baseUrl("https://restcountries.eu/rest/v2/")
        .addConverterFactory(GsonConverterFactory.create())
        .client(httpClient)
        .build()

    private val COUNTRIES_LIST_FIELD_FILTER = "name;capital;alpha2Code"
    private val COUNTRY_DET_FIELD_FILTER = "name;capital;alpha2Code;region;subregion;population;languages"

    private val webService:Ws

    init {
        webService = rClient.create(Ws::class.java)
    }

    override suspend fun getAllWorldCountry():ArrayList<CountryDTO>? {
        return webService.getAllCountry(COUNTRIES_LIST_FIELD_FILTER).execute().body()
    }

    override suspend fun getWorldCountryByLanguage( languageCode: String): ArrayList<CountryDTO>? {
        return webService.getCountryByLanguage(languageCode,COUNTRIES_LIST_FIELD_FILTER).execute().body()
    }

    override suspend fun getWorldCountryByContinent( continentCode: String): ArrayList<CountryDTO>? {
        return webService.getCountryByContinents(continentCode,COUNTRIES_LIST_FIELD_FILTER).execute().body()
    }

    override suspend fun getCountry( countryCode: String): CountryDetailDTO? {
        return webService.getCountryDet(countryCode,COUNTRY_DET_FIELD_FILTER).execute().body()
    }

    companion object{
        val instance = ClientImp()
    }
}