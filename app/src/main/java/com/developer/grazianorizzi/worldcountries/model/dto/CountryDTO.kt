package com.developer.grazianorizzi.worldcountries.model.dto

import com.google.gson.annotations.SerializedName

data class CountryDTO (
    @SerializedName("name") var name:String,
    @SerializedName("alpha2Code") var countryCode:String,
    @SerializedName("capital") var capital:String)