package com.developer.grazianorizzi.worldcountries.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.developer.grazianorizzi.worldcountries.R
import com.developer.grazianorizzi.worldcountries.entity.Country
import com.developer.grazianorizzi.worldcountries.view.fragments.CountriesFragment
import com.developer.grazianorizzi.worldcountries.view.fragments.CountryDetailFragment

class CountriesListActivity : AppCompatActivity(),
    CountriesFragment.OnListFragmentInteractionListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.countries_list_activity)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right)
                .replace(R.id.container, CountriesFragment.newInstance())
                .commit()
        }
    }

    override fun onListFragmentInteraction(item: Country?) {
        supportFragmentManager.beginTransaction()
            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,R.anim.enter_from_left, R.anim.exit_to_right)
            .replace(R.id.container, CountryDetailFragment.newInstance(item))
            .addToBackStack(null)
            .commit()
    }

}
