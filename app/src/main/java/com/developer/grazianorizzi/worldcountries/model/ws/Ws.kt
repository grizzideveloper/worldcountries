package com.developer.grazianorizzi.worldcountries.model.ws


import com.developer.grazianorizzi.worldcountries.model.dto.CountryDTO
import com.developer.grazianorizzi.worldcountries.model.dto.CountryDetailDTO
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface Ws {

    @GET("all")
    fun getAllCountry(@Query("fields") filter:String): Call<ArrayList<CountryDTO>>

    @GET("lang/{id}")
    fun getCountryByLanguage(@Path("id") id:String,@Query("fields")fields:String): Call<ArrayList<CountryDTO>>

    @GET("region/{id}")
    fun getCountryByContinents(@Path("id") id:String,@Query("fields")fields:String): Call<ArrayList<CountryDTO>>

    @GET("alpha/{id}")
    fun getCountryDet(@Path("id") countryCode:String,@Query("fields")fields:String): Call<CountryDetailDTO>
}