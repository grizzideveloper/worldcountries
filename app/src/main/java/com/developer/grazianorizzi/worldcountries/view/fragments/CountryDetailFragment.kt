package com.developer.grazianorizzi.worldcountries.view.fragments


import android.app.Application
import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateViewModelFactory
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.developer.grazianorizzi.worldcountries.R
import com.developer.grazianorizzi.worldcountries.entity.Country
import com.developer.grazianorizzi.worldcountries.utils.ErrorHandler
import com.developer.grazianorizzi.worldcountries.utils.ImageLoader
import com.developer.grazianorizzi.worldcountries.view.adapters.MyLanguageRecyclerViewAdapter
import com.developer.grazianorizzi.worldcountries.viewmodel.CountryDetailViewModel
import com.developer.grazianorizzi.worldcountries.viewmodel.LoadableViewModel
import kotlinx.android.synthetic.main.country_detail_fragment.*


class CountryDetailFragment : Fragment() {

    companion object {
        fun newInstance(ctr:Country?) = CountryDetailFragment().apply {
            country = ctr
        }
    }

    private lateinit var viewModel: CountryDetailViewModel
    private var country: Country? = null
    private lateinit var progressDialog: ProgressDialog
    private lateinit var errorHandler: ErrorHandler

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.country_detail_fragment, container, false)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, SavedStateViewModelFactory(context?.applicationContext as Application,this)).get(CountryDetailViewModel::class.java)
        progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Loading")
        progressDialog.setCancelable(false)

        errorHandler = ErrorHandler(context)

        viewModel.state.observe(this, Observer {
            if (it == LoadableViewModel.ViewModelState.LOADING){
                progressDialog.show()
            }else{
                progressDialog.hide()
            }
        })

        viewModel.country.observe(this, Observer {
            country_name.text = it.name
            country_population.text = it.population.toString()
            capital_name.text = it.capital
            country_region.text = it.region
            ImageLoader.loadCountryFlag(country_flag,it.countryCode)
            with(language_list){
                layoutManager = LinearLayoutManager(context)
                adapter = MyLanguageRecyclerViewAdapter(it.languages,null)
            }
        })

        viewModel.errorRecived.observe(this, Observer {
            //Show error
            it.consume()?.let {
                errorHandler.handleError(it)
            }
        })

        country?.let {
            viewModel.loadCountryDetail(it)
        }
    }

}
