package com.developer.grazianorizzi.worldcountries.entity

import java.io.Serializable


data class CountryDetail(
    var name: String,
    var countryCode: String,
    var capital: String,
    var region: String,
    var subregion: String,
    var population: Int,
    var languages: ArrayList<Language>):Serializable