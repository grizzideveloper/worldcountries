package com.developer.grazianorizzi.worldcountries.view

import android.content.Context
import android.util.AttributeSet
import android.widget.Spinner

class MySpinner(context: Context,attrs:AttributeSet):Spinner(context,attrs) {

    var itemSelectionListener:ItemSelection? = null


    override fun setSelection(position: Int) {
        super.setSelection(position)
        itemSelectionListener?.let {
            it.onItemSelected(position)
        }
    }

    interface ItemSelection{
        fun onItemSelected(position: Int)
    }
}