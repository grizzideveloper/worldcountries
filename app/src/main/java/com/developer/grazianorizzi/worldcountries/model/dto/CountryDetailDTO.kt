package com.developer.grazianorizzi.worldcountries.model.dto

import com.google.gson.annotations.SerializedName


data class CountryDetailDTO(
    @SerializedName("name") var name: String,
    @SerializedName("alpha2Code") var countryCode: String,
    @SerializedName("capital") var capital: String,
    @SerializedName("region") var region: String,
    @SerializedName("subregion") var subregion: String,
    @SerializedName("population") var population: Int,
    @SerializedName("languages") var languages: ArrayList<LanguageDTO>)