package com.developer.grazianorizzi.worldcountries.model.dto

import com.google.gson.annotations.SerializedName

data class LanguageDTO(
    @SerializedName("name") var languageName:String
)