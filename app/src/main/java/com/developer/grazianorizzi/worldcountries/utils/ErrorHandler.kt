package com.developer.grazianorizzi.worldcountries.utils

import android.content.Context
import androidx.appcompat.app.AlertDialog
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class ErrorHandler(private val context: Context?) {

    fun handleError(t:Throwable){
        when(t){
            is ReadLocalDataException ->{
                showDialog("Errore","Impossibile leggere dati")
            }
            is UnknownHostException -> {
                showDialog("Errore","Errore di rete, controllare la connessione a internet")
            }
            is SocketTimeoutException -> {
                showDialog("Errore","Errore di rete, controllare la connessione a internet")
            }
            else -> {
                showDialog("Errore","Ops, c'è stato un errore")
            }
        }
    }

    private fun  showDialog(title:String,message:String){
        context?.let {
            AlertDialog.Builder(it)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(android.R.string.yes) { dialog, which ->
                dialog.dismiss()
            }.show()
        }
    }
}