package com.developer.grazianorizzi.worldcountries.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.developer.grazianorizzi.worldcountries.R
import com.developer.grazianorizzi.worldcountries.entity.Filter

class MySpinnerAdapter(context: Context?, private var filters:List<Filter> = ArrayList()) : ArrayAdapter<Filter>(context,0,filters) {

    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val vh: ItemRowHolder

        if (convertView == null) {
            view = mInflater.inflate(R.layout.spinner_dropdown_item, parent, false)
            vh =
                ItemRowHolder(
                    view
                )
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemRowHolder
        }

        getItem(position)?.let { filter ->
            vh.label.text = filter.name
        }
        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val vh: ItemRowHolder

        if (convertView == null) {
            view = mInflater.inflate(R.layout.spinner_dropdown_item, parent, false)
            vh =
                ItemRowHolder(
                    view
                )
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemRowHolder
        }

        getItem(position)?.let { filter ->
            vh.label.text = filter.name
        }
        return view
    }



    private class ItemRowHolder(row: View?) {

        val label: TextView = row?.findViewById(R.id.spinner_text) as TextView
    }

}