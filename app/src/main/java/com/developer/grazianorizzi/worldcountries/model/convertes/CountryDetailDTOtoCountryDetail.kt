package com.developer.grazianorizzi.worldcountries.model.convertes

import com.developer.grazianorizzi.worldcountries.entity.CountryDetail
import com.developer.grazianorizzi.worldcountries.entity.Language
import com.developer.grazianorizzi.worldcountries.model.dto.CountryDetailDTO

class CountryDetailDTOtoCountryDetail {

    fun convert(cdetail:CountryDetailDTO): CountryDetail {
        val languages:ArrayList<Language>
        val languageConverter = LanguageDTOtoLanguage()
        languages = languageConverter.convert(cdetail.languages)
        return CountryDetail(cdetail.name,cdetail.countryCode,cdetail.capital,cdetail.region,cdetail.subregion,cdetail.population,languages)
    }
}