package com.developer.grazianorizzi.worldcountries.model.data

import com.developer.grazianorizzi.worldcountries.model.dto.FilterDTO

interface LocalRepository {
    suspend fun getFilterLanguage():Array<FilterDTO>
    suspend fun getFilterContinent():Array<FilterDTO>
}