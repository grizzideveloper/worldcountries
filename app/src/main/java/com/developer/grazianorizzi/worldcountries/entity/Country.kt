package com.developer.grazianorizzi.worldcountries.entity

import java.io.Serializable

data class Country (var name:String, var countryCode:String, var capital:String):Serializable