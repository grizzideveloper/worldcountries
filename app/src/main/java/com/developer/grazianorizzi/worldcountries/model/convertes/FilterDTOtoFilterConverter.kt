package com.developer.grazianorizzi.worldcountries.model.convertes

import com.developer.grazianorizzi.worldcountries.entity.Filter
import com.developer.grazianorizzi.worldcountries.model.dto.FilterDTO


class FilterDTOtoFilterConverter {

    fun convert(filters:Array<FilterDTO>):ArrayList<Filter>{
        val fils:ArrayList<Filter> = ArrayList()
        filters.forEach{
            fils.add(Filter(it.name,it.code))
        }
        return fils
    }
}