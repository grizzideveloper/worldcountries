package com.developer.grazianorizzi.worldcountries.utils

class ConsumableThrowable(private var error:Throwable? = null) {

    fun consume():Throwable?{
        val t = error
        error = null
        return t
    }
}