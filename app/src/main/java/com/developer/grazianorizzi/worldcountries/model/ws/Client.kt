package com.developer.grazianorizzi.worldcountries.model.ws
import com.developer.grazianorizzi.worldcountries.model.dto.CountryDTO
import com.developer.grazianorizzi.worldcountries.model.dto.CountryDetailDTO

interface Client {
    suspend fun getAllWorldCountry():ArrayList<CountryDTO>?
    suspend fun getWorldCountryByLanguage(languageCode:String):ArrayList<CountryDTO>?
    suspend fun getWorldCountryByContinent(continentCode:String):ArrayList<CountryDTO>?
    suspend fun getCountry(countryCode:String): CountryDetailDTO?
}