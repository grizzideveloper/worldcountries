package com.developer.grazianorizzi.worldcountries.model.convertes

import com.developer.grazianorizzi.worldcountries.entity.Language
import com.developer.grazianorizzi.worldcountries.model.dto.LanguageDTO

class LanguageDTOtoLanguage {

    fun convert(lang:LanguageDTO): Language {
        return Language(lang.languageName)
    }

    fun convert(lang:ArrayList<LanguageDTO>): ArrayList<Language> {
        val fils:ArrayList<Language> = ArrayList()
        lang.forEach{
            fils.add(Language(it.languageName))
        }
        return fils
    }
}