package com.developer.grazianorizzi.worldcountries.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.developer.grazianorizzi.worldcountries.utils.ConsumableThrowable
import kotlinx.coroutines.launch

open class LoadableViewModel : ViewModel() {

    private var launchCount = 0

    val state: MutableLiveData<ViewModelState> = MutableLiveData()
    val errorRecived: MutableLiveData<ConsumableThrowable> = MutableLiveData()

    fun launchOnViewModelScope(job:suspend ()->Unit){
        viewModelScope.launch {
            try {
                newLaunch()
                job()
                finishLaunch()
            }catch (e:Throwable){
                finishLaunch()
                errorRecived.value = ConsumableThrowable(e)
            }
        }
    }

    private fun newLaunch(){
        launchCount++
        state.value = ViewModelState.LOADING
    }

    private fun finishLaunch(){
        launchCount--
        if (launchCount == 0){
            state.value = ViewModelState.LOADED
        }
    }

    enum class ViewModelState{
        LOADING,
        LOADED
    }
}