package com.developer.grazianorizzi.worldcountries.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.developer.grazianorizzi.worldcountries.entity.Country
import com.developer.grazianorizzi.worldcountries.entity.CountryDetail
import com.developer.grazianorizzi.worldcountries.model.CountryDetailModel

class CountryDetailViewModel(private val savedState: SavedStateHandle)  : LoadableViewModel() {

    private val countryDetailModel:CountryDetailModel = CountryDetailModel()

    val country:MutableLiveData<CountryDetail> = savedState.getLiveData("COUNTRY")


    fun loadCountryDetail(country:Country){
        if (this.country.value?.countryCode != country.countryCode)
        {
            launchOnViewModelScope {
                this.country.value = countryDetailModel.getCountryDetail(country)
                savedState.set("COUNTRY",this.country.value)
            }
        }
    }
}
