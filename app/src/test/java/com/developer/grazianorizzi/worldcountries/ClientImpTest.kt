package com.developer.grazianorizzi.worldcountries

import com.developer.grazianorizzi.worldcountries.model.dto.CountryDetailDTO
import com.developer.grazianorizzi.worldcountries.model.dto.LanguageDTO
import com.developer.grazianorizzi.worldcountries.model.ws.Client
import com.developer.grazianorizzi.worldcountries.model.ws.ClientImp
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class ClientImpTest {

    val client:Client = ClientImp.instance

    @Test
    fun getAllContries(){
        runBlocking {
            val countries =  client.getAllWorldCountry()
            Assert.assertEquals(countries!!.size,250)
        }
    }

    @Test
    fun getItalyDetail(){
        runBlocking {
            val country =  client.getCountry("it")
            val lang = ArrayList<LanguageDTO>()
            lang.add(LanguageDTO("Italian"))
            val italy = CountryDetailDTO("Italy","IT","Rome","Europe","Southern Europe",60665551,lang)
            Assert.assertEquals(country,italy)
        }
    }

    @Test
    fun getCountriesWrongLanguageCode(){
        runBlocking {
            val countries = client.getWorldCountryByLanguage("pippo")
            Assert.assertNull(countries)
        }
    }

    @Test
    fun getCountriesLanguageCode(){
        runBlocking {
            val countries = client.getWorldCountryByLanguage("it")
            Assert.assertEquals(countries!!.size,4)
        }
    }
}