package com.developer.grazianorizzi.worldcountries

import com.developer.grazianorizzi.worldcountries.model.convertes.CountryDTOtoCountryConverter
import com.developer.grazianorizzi.worldcountries.model.dto.CountryDTO
import org.junit.Assert
import org.junit.Test

class CountryDTOtoCountryConverterTest {

    val countryDTOtoCountryConverter:CountryDTOtoCountryConverter = CountryDTOtoCountryConverter()

    @Test
    fun testListaVuota(){
        val countriesDTO:ArrayList<CountryDTO> = ArrayList()
        val countries = countryDTOtoCountryConverter.convert(countriesDTO)
        Assert.assertTrue(countries.isEmpty())
    }

    //Mi aspetto che le due liste abbiano lo stesso numero di elementi
    @Test
    fun testStessoNumeroDiElementi(){
        val countriesDTO:ArrayList<CountryDTO> = ArrayList()
        countriesDTO.add(CountryDTO("Italy","IT","Roma"))
        countriesDTO.add(CountryDTO("Germany","DE","Berlin"))
        countriesDTO.add(CountryDTO("","",""))
        val countries = countryDTOtoCountryConverter.convert(countriesDTO)
        Assert.assertEquals(countries.count(),countriesDTO.count())
    }
}