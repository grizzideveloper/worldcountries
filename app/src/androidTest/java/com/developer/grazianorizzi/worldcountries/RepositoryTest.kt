package com.developer.grazianorizzi.worldcountries

import androidx.test.runner.AndroidJUnit4
import com.developer.grazianorizzi.worldcountries.model.data.LocalRepositoryImp
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class RepositoryTest {

    val rep = LocalRepositoryImp()

    @Test
    fun testLanguageFilter(){
        runBlocking {
            val a = rep.getFilterLanguage()
            Assert.assertEquals(a.size,185)
        }
    }
}