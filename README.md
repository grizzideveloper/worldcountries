README

Librerire utilizzate:

Retrofit :  Libreria molto utilizzata per consumare servizi REST, grazie alle annotation e alla compatibilità con la libreria Gson permette di parsare direttamente le risposte in json in classi utilizzando sempre le annotation.

Gson: libreria che appunto serve a parsare stringhe o risposte da servizi rest direttamente in classi grazie alle annotation.

OkHttp: compatibile con retrofit permette di impostare valori come i timeout per la connessione, interceptor per intercettare le chiamate ed è compatibile con il profile di Android Studio per l'ispezione del network

Picasso: Lireria utile per caricare in maniera asincrona delle immagini da remoto, avendo una funzione di caching riduce il traffico



Pattern:

Il patter untilizzato per il progetto è il MVVM, utilizza i componenti architetturali andoid come ViewModel e LiveData.

Per il mantenimento dello stato attuale dei fragment è stata utilizzata la libreria lifecycle-viewmodel-savedstate che permette ai viewmodel di conservare lo stato anche nel caso di kill del fragment/activity da parte del sistema operativo oltre che ai cambi di rotazione.

Per il filtraggio per lingua dato che il servizio REST fornito necessitava del ISO 639-1 e trovavo poco user friendly far digitare all'utente un tale codice, che probabilment non conosce, ho optato per una selezione da una lista (Spinner) per tanto ho caricato un json che contiene i codici ISO 639-1 correlati al nome della lingua in modo che si possa scegliere questa invece che digitarne il codice.
Per omogeneità ho utilizzato lo stesso sistema con il filtraggio per continente.

Una delle altre possibilità era quella di creare un vero e proprio DB (Room?) ma l'ho trovato eccessivo per questo progetto di esempio anche perchè si tratta di dati invarianti, oppure si potevano ricavare codici e nomi caricando l'intersa lista delle nazioni dal servizio Rest, ciclando e rimuovendo le occorrenze ripetute ma implicava un maggior dispendio a livello di traffico dati e di elaborazione.

Nel caso di evoluzioni del progetto stesso è possibile sostituire l'implementazione del LocalRepository ed utilizzare un DB in locale.

Ho inserito delle classi di conversione anche se in questo caso non servono solitamente viene creato uno strato di conversione tra i DTO provenienti dal servizio e le entità restituite dai vari Model.